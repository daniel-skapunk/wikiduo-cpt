<?php
/*
Plugin Name: Wiki Duo Custom Post Type
Description: Custom Post Types for "Curioso Circo" website.
Author: Daniel Duarte
Author URI: https://github.com/danielskapunk/
Version: 1.2
*/

add_action( 'init', 'wiki_cpt' );

function wiki_cpt() {

register_post_type( 'wikiduo', array(
  'labels' => array(
    'name' => 'Wiki Duo',
    'singular_name' => 'wiki duo',
   ),
  'has_archive' => 'wikiduo',
  'description' => 'Wiki Duo posts',
  'public' => true,
  'menu_position' => 20,
  'supports' => array( 'title', 'editor', 'custom-fields', 'comments' )
));
}